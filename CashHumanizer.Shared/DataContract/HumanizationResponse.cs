﻿using System.Runtime.Serialization;

namespace CashHumanizer.Shared.DataContract
{
    [DataContract]
    public class HumanizationResponse
    {
        [DataMember]
        public bool Successed { get; set; }
        [DataMember]
        public string HumanizedCash { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }
    }
}
