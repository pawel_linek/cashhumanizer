﻿using System.Collections.Generic;
using CashHumanizer.Service.Components.Implementation;
using NUnit.Framework;

namespace CashHumanizer.Service.Test.AcceptanceCriteriaTests
{
    [TestFixture]
    public class CashHumanizerComponentAcceptanceCriteriaTest : TestWithSubject<CashHumanizerComponent>
    {
        protected override void AssignSubject()
        {
            Subject = new CashHumanizerComponent(new Service.Components.Implementation.CashHumanizer(), new InputValidator(), new CashParser());
        }

        [TestCaseSource(nameof(AcceptanceCriterias))]
        public void TestAcceptanceCriteria(AcceptanceCriteria criteria)
        {
            var output = Subject.HumanizeCash(criteria.Input);

            Assert.IsTrue(output.Successed);
            Assert.AreEqual(criteria.Output, output.HumanizedContent);
        }

        [Test]
        public void OnlyCommaIsAllowedAsDecimalSign()
        {
            const string inputWithWrongDecimalSeparator = "123.50";

            var output = Subject.HumanizeCash(inputWithWrongDecimalSeparator);

            Assert.IsFalse(output.Successed);
        }

        private static IEnumerable<AcceptanceCriteria> AcceptanceCriterias()
        {
            yield return new AcceptanceCriteria("0", "zero dollars");
            yield return new AcceptanceCriteria("1", "one dollar");
            yield return new AcceptanceCriteria("25,10", "twenty-five dollars and ten cents");

            //This one comes from the PDF delivered to me, but looks like this one is wrong.
            //From mathematical point of view the 0,1$ is ten cents, not a one cent
            //
            //yield return new AcceptanceCriteria("0,1", "zero dollars and one cent");

            yield return new AcceptanceCriteria("0,1", "zero dollars and ten cents");
            yield return new AcceptanceCriteria("0,01", "zero dollars and one cent");

            yield return new AcceptanceCriteria("45100", "forty-five thousand one hundred dollars");
            yield return new AcceptanceCriteria("999999999,99",
                "nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine dollars and ninety-nine cents");
        }

        public class AcceptanceCriteria
        {
            public AcceptanceCriteria(string input, string output)
            {
                Input = input;
                Output = output;
            }

            public string Input { get; }
            public string Output { get; }
        }
    }
}