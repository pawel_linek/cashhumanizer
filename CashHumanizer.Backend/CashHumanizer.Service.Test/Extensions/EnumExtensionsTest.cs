﻿using CashHumanizer.Service.Enums;
using CashHumanizer.Service.Extensions;
using NUnit.Framework;

namespace CashHumanizer.Service.Test.Extensions
{
    [TestFixture]
    public class EnumExtensionsTest
    {
        [Test]
        public void Test()
        {
            Assert.AreEqual("million", CashRank.Million.ToLower());
        }
    }
}