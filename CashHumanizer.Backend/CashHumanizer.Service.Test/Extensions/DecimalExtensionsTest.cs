﻿using CashHumanizer.Service.Extensions;
using NUnit.Framework;

namespace CashHumanizer.Service.Test.Extensions
{
    [TestFixture]
    public class DecimalExtensionsTest
    {
        private const decimal Cash = 12.33m;

        [Test]
        public void GetDollarsTest()
        {
            var dollars = Cash.GetDollars();

            Assert.AreEqual(12, dollars);
        }

        [Test]
        public void GetCentsTest()
        {
            var dollars = Cash.GetDollars();

            var cents = Cash.GetCents(dollars);

            Assert.AreEqual(33, cents);
        }
    }
}
