﻿using AutoMock;
using NUnit.Framework;

namespace CashHumanizer.Service.Test
{
    public abstract class TestWithSubject<T> where T: class
    {
        protected T Subject;
        protected RhinoAutoMocker<T> AutoMocker;

        [SetUp]
        public virtual void SetUp()
        {
            AutoMocker = new RhinoAutoMocker<T>();
            AssignSubject();
        }

        protected virtual void AssignSubject()
        {
            Subject = AutoMocker.ClassUnderTest;
        }

        [TearDown]
        public virtual void TearDown()
        {
            AutoMocker.Repository.VerifyAll();
        }

    }
}