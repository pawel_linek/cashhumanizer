﻿using CashHumanizer.Service.Components.Implementation;
using CashHumanizer.Service.Enums;
using NUnit.Framework;

namespace CashHumanizer.Service.Test.Components
{
    [TestFixture]
    public class InputValidatorTest : TestWithSubject<InputValidator>
    {
        [TestCase(-1, ActionStatus.Failed)]
        [TestCase(12, ActionStatus.Ok)]
        [TestCase(1000000000, ActionStatus.Failed)]
        public void InputValidationTest(decimal input, ActionStatus expectedValidationStatus)
        {
            var validationResult = Subject.Check(input);
            Assert.AreEqual(expectedValidationStatus, validationResult.Status);
        }
    }
}