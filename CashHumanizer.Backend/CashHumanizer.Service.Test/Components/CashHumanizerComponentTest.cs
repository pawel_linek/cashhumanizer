﻿using System;
using System.Globalization;
using CashHumanizer.Service.Components.Implementation;
using CashHumanizer.Service.Components.Interface;
using CashHumanizer.Service.Enums;
using CashHumanizer.Service.Extensions;
using CashHumanizer.Service.Model;
using FizzWare.NBuilder;
using NUnit.Framework;
using Rhino.Mocks;

namespace CashHumanizer.Service.Test.Components
{
    [TestFixture]
    public class CashHumanizerComponentTest : TestWithSubject<CashHumanizerComponent>
    {
        [Test]
        public void InputParsingCashHumanizerTest()
        {
            const string notACashInput = "ThisIsNotACash";
            var parsingResult = Builder<ParsingResult>.CreateNew()
                .With(r => r.ParsingStatus = ActionStatus.Failed)
                .Build();
            AutoMocker.Get<ICashParser>().Stub(p => p.Parse(notACashInput)).Return(parsingResult);

            var result = Subject.HumanizeCash(notACashInput);

            Assert.IsFalse(result.Successed);
        }

        [Test]
        public void InputValidationCashHumanizerTest()
        {
            const decimal lessThanZero = -10;
            var lessTahanZeroInput = lessThanZero.ToString(CultureInfo.InvariantCulture);
            var parsingResult = Builder<ParsingResult>.CreateNew()
                .With(r => r.ParsingStatus = ActionStatus.Ok)
                .With(r => r.Result = lessThanZero)
                .Build();
            AutoMocker.Get<ICashParser>().Stub(p => p.Parse(lessTahanZeroInput)).Return(parsingResult);

            var preCheckResult = Builder<PreCheckResult>.CreateNew().With(r => r.Status = ActionStatus.Failed).Build();
            AutoMocker.Get<IInputValidator>().Stub(v => v.Check(lessThanZero)).Return(preCheckResult);

            var result = Subject.HumanizeCash(lessTahanZeroInput);

            Assert.IsFalse(result.Successed);
        }

        [Test]
        public void HumanizeDollarsOnlySuccess()
        {
            const decimal someCash = 10;
            const string expected = "Ten dollars";
            StubValidationForSuccess(someCash);
            AutoMocker.Get<ICashHumanizer>().Stub(h => h.HumanizeCash(someCash.ToInt())).Return("Ten");

            var result = Subject.HumanizeCash(someCash.ToString(CultureInfo.InvariantCulture));

            Assert.AreEqual(expected, result.HumanizedContent);
        }

        [Test]
        public void HumanizeDollarsWithCentsSuccess()
        {
            const decimal someCash = 10.10m;
            const string expected = "Ten dollars and Ten cents";
            StubValidationForSuccess(someCash);
            AutoMocker.Get<ICashHumanizer>().Stub(h => h.HumanizeCash(someCash.ToInt())).Return("Ten");

            var result = Subject.HumanizeCash(someCash.ToString(CultureInfo.InvariantCulture));

            Assert.AreEqual(expected, result.HumanizedContent);
        }

        private void StubValidationForSuccess(decimal someCash)
        {
            var someCashInput = someCash.ToString(CultureInfo.InvariantCulture);
            var parsingResult = Builder<ParsingResult>.CreateNew()
                .With(r => r.ParsingStatus = ActionStatus.Ok)
                .With(r => r.Result = someCash)
                .Build();
            AutoMocker.Get<ICashParser>().Stub(p => p.Parse(someCashInput)).Return(parsingResult);

            var preCheckResult = Builder<PreCheckResult>.CreateNew().With(r => r.Status = ActionStatus.Ok).Build();
            AutoMocker.Get<IInputValidator>().Stub(v => v.Check(someCash)).Return(preCheckResult);
        }
    }
}