﻿using CashHumanizer.Service.Components.Implementation;
using CashHumanizer.Service.Enums;
using NUnit.Framework;

namespace CashHumanizer.Service.Test.Components
{
    [TestFixture]
    public class CashParserTest : TestWithSubject<CashParser>
    {
        [TestCase("ThisIsNotACash", ActionStatus.Failed)]
        [TestCase("100.98", ActionStatus.Failed)]
        [TestCase("100,98", ActionStatus.Ok)]
        public void ParseTest(string input, ActionStatus isSuccessedExpectation)
        {
            var parsingResult = Subject.Parse(input);
            Assert.AreEqual(isSuccessedExpectation, parsingResult.ParsingStatus);
        }
    }
}