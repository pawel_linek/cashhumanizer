﻿using System;
using System.ServiceModel;
using CashHumanizer.Service.AsyncResult;
using CashHumanizer.Service.Bootstrap;
using CashHumanizer.Service.Components.Interface;
using CashHumanizer.Service.Model;
using CashHumanizer.Shared.DataContract;

namespace CashHumanizer.Service.ServiceContract
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class CashHumanizerService : ICashHumanizerService
    {
        private static ICashHumanizerComponent CashHumanizerComponent => 
            DependencyContainer.Resolve<ICashHumanizerComponent>();

        public CashHumanizerService()
        {
            // In the non-self hosted WCF services there is a possibility to have Service Factory
            // Where we can trigger the bootstrapping to initialize DI container, Automapper configuration, etc.
            // Then in this case there will be chance to inject Component via constructor - not like here using service locator
            Bootstrapper.Configure();
        }

        public IAsyncResult BeginCashToHumanize(string cash, AsyncCallback callback, object asyncState)
        {
            var humanizedCash = CashHumanizerComponent.HumanizeCash(cash);
            return new SimplyAsyncResult<HumanizationResult>(humanizedCash);
        }

        public HumanizationResponse EndCashToHumanize(IAsyncResult asyncResult)
        {
            var result = asyncResult as SimplyAsyncResult<HumanizationResult>;
            if (result?.Data == null)
            {
                return new HumanizationResponse {ErrorMessage = "Async Result is empty"};
            }

            return result.Data.Successed == false
                ? new HumanizationResponse {ErrorMessage = result.Data.IssueDescription}
                : new HumanizationResponse
                {
                    HumanizedCash = result.Data.HumanizedContent,
                    Successed = result.Data.Successed
                };
        }
    }
}
