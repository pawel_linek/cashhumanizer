﻿using System;
using System.ServiceModel;
using CashHumanizer.Shared.DataContract;

namespace CashHumanizer.Service.ServiceContract
{
    [ServiceContract]
    public interface ICashHumanizerService
    {
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCashToHumanize(string cash, AsyncCallback callback, object asyncState);

        HumanizationResponse EndCashToHumanize(IAsyncResult asyncResult);
    }
}