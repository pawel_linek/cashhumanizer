﻿namespace CashHumanizer.Service.Extensions
{
    public static class StringExtensions
    {
        public static string Ieth(this string self)
        {
            return  $"{self.TrimEnd('y')}ieth";
        }

        public static string Th(this string self)
        {
            return $"{self}th";
        }

        public static string Pluralize(this string self, int value)
        {
            var isPluralizationNeeded = value > 1 || value == 0;
            return $"{self}{(isPluralizationNeeded ? "s" : string.Empty)}";
        }
    }
}
