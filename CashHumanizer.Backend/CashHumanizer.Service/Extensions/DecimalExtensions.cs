﻿using System;

namespace CashHumanizer.Service.Extensions
{
    public static class DecimalExtensions
    {
        public static decimal GetCents(this decimal value, decimal dollars)
        {
            return (value - dollars) * 100;
        }

        public static decimal GetDollars(this decimal value)
        {
            return Math.Truncate(value);
        }

        public static int ToInt(this decimal value)
        {
            return decimal.ToInt32(value);
        }

        public static bool Between(this decimal value, decimal min, decimal max)
        {
            return value >= min && value <= max;
        }
    }
}
