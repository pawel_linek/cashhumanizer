﻿using System;
using CashHumanizer.Service.Enums;

namespace CashHumanizer.Service.Extensions
{
    public static class EnumExtensions
    {
        public static string ToLower(this Enum self)
        {
            return self.ToString().ToLower();
        }

        public static int AsInt(this CashRank self)
        {
            return (int)self;
        }
    }
}