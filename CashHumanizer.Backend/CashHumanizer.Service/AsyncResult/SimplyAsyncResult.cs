﻿using System;
using System.Threading;

namespace CashHumanizer.Service.AsyncResult
{
    /// <summary>
    /// This is simple implemenatation of the IAsync result pattern.
    /// To have fully implementend IAsyncResult pattern to handle waits, exceptions etc please reffer to this MSDN blog article
    /// https://blogs.msdn.microsoft.com/nikos/2011/03/14/how-to-implement-the-iasyncresult-design-pattern/
    /// </summary>
    /// <typeparam name="T">Data content</typeparam>
    public class SimplyAsyncResult<T> : IAsyncResult
    {
        public SimplyAsyncResult(T data)
        {
            Data = data;
        }

        public T Data { get; }

        public object AsyncState => Data;

        public WaitHandle AsyncWaitHandle => null;

        public bool CompletedSynchronously => true;

        public bool IsCompleted => true;
    }
}
