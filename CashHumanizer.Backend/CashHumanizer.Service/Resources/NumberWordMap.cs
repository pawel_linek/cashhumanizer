﻿using System.Collections.Generic;

namespace CashHumanizer.Service.Resources
{
    /// <summary>
    /// This can be a part of the resources so playing with other languages will be possible
    /// </summary>
    public static class NumberWordMap
    {
        public static IEnumerable<string> Units => new List<string>
        {
            "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve",
            "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
        };

        public static IEnumerable<string> Tens => new List<string>
            {"zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    }
}