﻿using System.Linq;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;

namespace CashHumanizer.Service.Bootstrap
{
    public static class DependencyContainer
    {
        private static readonly IWindsorContainer Container = new WindsorContainer();

        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }

        public static void Register()
        {
            Container.Kernel.Resolver.AddSubResolver(new CollectionResolver(Container.Kernel));

            var myNamespace = typeof(Bootstrapper).Namespace?.Split('.').First() ?? string.Empty;
            Container.Register(Classes.FromThisAssembly()
                .AllowMultipleMatches()
                .Where(t => t.Namespace != null && t.Namespace.StartsWith(myNamespace))
                .WithService.AllInterfaces()
                .LifestyleTransient());
        }
    }
}
