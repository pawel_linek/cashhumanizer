﻿namespace CashHumanizer.Service.Bootstrap
{
    public static class Bootstrapper
    {
        public static void Configure()
        {
            DependencyContainer.Register();
        }
    }
}