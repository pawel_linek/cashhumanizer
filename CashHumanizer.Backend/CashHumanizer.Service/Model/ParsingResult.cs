﻿using CashHumanizer.Service.Enums;

namespace CashHumanizer.Service.Model
{
    public class ParsingResult
    {
        public ActionStatus ParsingStatus { get; set; }
        public decimal? Result { get; set; }
        public string Description { get; set; }
    }
}