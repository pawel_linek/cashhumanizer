﻿namespace CashHumanizer.Service.Model
{
    public class HumanizationResult
    {
        public string HumanizedContent { get; set; }
        public bool Successed { get; set; }
        public string IssueDescription { get; set; }
    }
}