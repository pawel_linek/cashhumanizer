﻿using CashHumanizer.Service.Enums;

namespace CashHumanizer.Service.Model
{
    public class PreCheckResult
    {
        public ActionStatus Status { get; set; }
        public string Description { get; set; }
    }
}