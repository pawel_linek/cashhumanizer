﻿namespace CashHumanizer.Service.Enums
{
    public enum CashType
    {
        Dollar,
        Cent
    }
}