﻿namespace CashHumanizer.Service.Enums
{
    public enum CashRank
    {
        Unit = 1,
        Tens = 10,
        Hundred = 100,
        Thousand = 1000,
        Million = 1000000
    }
}