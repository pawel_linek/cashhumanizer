﻿using CashHumanizer.Service.Model;

namespace CashHumanizer.Service.Components.Interface
{
    public interface ICashParser
    {
        ParsingResult Parse(string cash);
    }
}