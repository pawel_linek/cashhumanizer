﻿namespace CashHumanizer.Service.Components.Interface
{
    public interface ICashHumanizer
    {
        string HumanizeCash(int value);
    }
}