﻿using CashHumanizer.Service.Model;

namespace CashHumanizer.Service.Components.Interface
{
    public interface ICashHumanizerComponent
    {
        HumanizationResult HumanizeCash(string cash);
    }
}