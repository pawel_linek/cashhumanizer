﻿using CashHumanizer.Service.Enums;
using CashHumanizer.Service.Model;

namespace CashHumanizer.Service.Components.Interface
{
    public interface IInputValidator
    {
        PreCheckResult Check(decimal cash);
    }
}