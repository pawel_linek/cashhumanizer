﻿using CashHumanizer.Service.Components.Interface;
using CashHumanizer.Service.Enums;
using CashHumanizer.Service.Extensions;
using CashHumanizer.Service.Model;

namespace CashHumanizer.Service.Components.Implementation
{
    public class InputValidator : IInputValidator
    {
        private const decimal Min = 0;
        private const decimal Max = 999999999.99m;

        public PreCheckResult Check(decimal cash)
        {
            var status = cash.Between(Min, Max) ? ActionStatus.Ok : ActionStatus.Failed;
            var desription = status == ActionStatus.Failed ? GetValidationIssueDescription(cash) : string.Empty;

            return new PreCheckResult {Status = status, Description = desription};
        }

        private static string GetValidationIssueDescription(decimal cash)
        {
            return $"Provided cash: {cash} needs to be between: {Min} and {Max}";
        }
    }
}