﻿using System.Collections.Generic;
using System.Linq;
using CashHumanizer.Service.Components.Interface;
using CashHumanizer.Service.Enums;
using CashHumanizer.Service.Extensions;
using CashHumanizer.Service.Resources;

namespace CashHumanizer.Service.Components.Implementation
{
    public class CashHumanizer : ICashHumanizer
    {
        private static IEnumerable<CashRank> BaseCashRanks => new List<CashRank> {CashRank.Million, CashRank.Thousand, CashRank.Hundred};
        private static int UnitsRange => Units.Length;
        private static string[] Units => NumberWordMap.Units.ToArray();
        private static string[] Tens => NumberWordMap.Tens.ToArray();
        private static string Zero => Units[0];

        public string HumanizeCash(int cash)
        {
            return HumanizeCash(cash: cash, isOrdinal: true);
        }

        private string Humanize(int cash)
        {
            return HumanizeCash(cash: cash, isOrdinal: false);
        }

        private string HumanizeCash(int cash, bool isOrdinal)
        {
            var humanizedParts = new List<string>();
            cash = HumanizeOneHundredToMilionRank(cash, humanizedParts);

            if (cash == 0 && humanizedParts.Any() == false)
            {
                return Zero;
            }

            if (cash == 0)
            {
                return JoinHumanizedParts(humanizedParts);
            }

            if (cash < UnitsRange)
            {
                HumanizeZeroToNintyRange(cash, humanizedParts);
            }
            else
            {
                HumanizeTwentyToNintyNineRange(cash, isOrdinal, humanizedParts);
            }

            return JoinHumanizedParts(humanizedParts);
        }

        private int HumanizeOneHundredToMilionRank(int cash, ICollection<string> humanizedParts)
        {
            return BaseCashRanks.Aggregate(cash, (current, cashRank) => HumanizeRank(current, cashRank, humanizedParts));
        }

        private static void HumanizeZeroToNintyRange(int cash, ICollection<string> humanizedParts)
        {
            var lessThanTwenty = Units[cash];
            humanizedParts.Add(lessThanTwenty);
        }

        private static string JoinHumanizedParts(IEnumerable<string> humanizedParts)
        {
            return string.Join(" ", humanizedParts);
        }

        private static void HumanizeTwentyToNintyNineRange(int cash, bool isOrdinal, ICollection<string> humanizedParts)
        {
            var tens = cash / 10;
            var lastPart = Tens[tens];

            var unitsPart = cash % 10;
            if (unitsPart > 0)
            {
                var humanizedUnit = Units[unitsPart];
                lastPart += $"-{humanizedUnit}";
            }
            else if (isOrdinal)
            {
                lastPart = lastPart.Ieth();
            }

            humanizedParts.Add(lastPart);
        }

        private int HumanizeRank(int cash, CashRank cashRank, ICollection<string> humanizedParts)
        {
            var cashRankNumber = cashRank.AsInt();
            var cashReducedtoLowerRank = cash / cashRankNumber;

            if (cashReducedtoLowerRank <= 0)
            {
                return cash;
            }

            var recurenceResult = Humanize(cashReducedtoLowerRank);
            var humanizedPart = $"{recurenceResult} {cashRank.ToLower()}";

            humanizedParts.Add(humanizedPart);

            var restCashToHumanize = cash % cashRankNumber;
            return restCashToHumanize;
        }
    }
}
