﻿using System.Globalization;
using CashHumanizer.Service.Components.Interface;
using CashHumanizer.Service.Enums;
using CashHumanizer.Service.Model;

namespace CashHumanizer.Service.Components.Implementation
{
    public class CashParser : ICashParser
    {
        private static readonly CultureInfo CultureWithComma = CultureInfo.GetCultureInfo("PL-pl");
        private const NumberStyles ValidNumberStyles = NumberStyles.AllowDecimalPoint |
                                                       NumberStyles.AllowThousands;

        public ParsingResult Parse(string cash)
        {
            decimal parsed;
            return decimal.TryParse(cash, ValidNumberStyles, CultureWithComma, out parsed)
                ? new ParsingResult {Result = parsed, ParsingStatus = ActionStatus.Ok}
                : new ParsingResult {Description = GetParsingIssueDescription (cash), ParsingStatus = ActionStatus.Failed};
        }

        private static string GetParsingIssueDescription(string cashCandidate)
        {
            return $"Input: [{cashCandidate}] cannot be converted to numeric cash value";
        }
    }
}