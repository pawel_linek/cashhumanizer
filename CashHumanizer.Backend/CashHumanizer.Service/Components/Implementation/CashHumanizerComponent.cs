﻿using CashHumanizer.Service.Components.Interface;
using CashHumanizer.Service.Enums;
using CashHumanizer.Service.Extensions;
using CashHumanizer.Service.Model;

namespace CashHumanizer.Service.Components.Implementation
{
    public class CashHumanizerComponent : ICashHumanizerComponent
    {
        private readonly ICashHumanizer _humanizer;
        private readonly IInputValidator _inputValidator;
        private readonly ICashParser _cashParser;

        public CashHumanizerComponent(ICashHumanizer humanizer, IInputValidator inputValidator, ICashParser cashParser)
        {
            _humanizer = humanizer;
            _inputValidator = inputValidator;
            _cashParser = cashParser;
        }

        public HumanizationResult HumanizeCash(string cash)
        {
            HumanizationResult humanizationResult;
            decimal parsedCash;
            if (CanContinue(cash, out parsedCash, out humanizationResult) == false)
            {
                return humanizationResult;
            }

            string humanizedPart;
            var dollars = HumanizeDollars(parsedCash, out humanizedPart);

            var cents = parsedCash.GetCents(dollars).ToInt();
            if (cents <= 0)
            {
                return GetSuccessedResult(humanizedPart);
            }

            string centsWord;
            var humanizedCents = HumanizedCents(cents, out centsWord);

            return GetSuccessedResult($"{humanizedPart} and {humanizedCents} {centsWord}");
        }

        private string HumanizedCents(int cents, out string centsWord)
        {
            var humanizedCents = _humanizer.HumanizeCash(cents);
            centsWord = CashType.Cent.ToLower().Pluralize(cents);
            return humanizedCents;
        }

        private int HumanizeDollars(decimal parsedCash, out string humanizedPart)
        {
            var dollars = parsedCash.GetDollars().ToInt();
            var dollarsWord = CashType.Dollar.ToLower().Pluralize(dollars);
            var humanizedDollars = _humanizer.HumanizeCash(dollars);
            humanizedPart = $"{humanizedDollars} {dollarsWord}";
            return dollars;
        }

        private bool CanContinue(string cash, out decimal parsedCash, out HumanizationResult humanizationResult)
        {
            parsedCash = 0;
            humanizationResult = null;

            var parsingResult = _cashParser.Parse(cash);
            var parsedCashCanidate = parsingResult.Result;
            if (parsingResult.ParsingStatus == ActionStatus.Failed || parsedCashCanidate == null)
            {
                humanizationResult = new HumanizationResult {Successed = false, IssueDescription = parsingResult.Description};
                return false;
            }

            parsedCash = parsedCashCanidate.Value;
            var validationResult = _inputValidator.Check(parsedCash);
            if (validationResult.Status == ActionStatus.Ok)
            {
                return true;
            }

            humanizationResult = new HumanizationResult { Successed = false, IssueDescription = validationResult.Description };
            return false;
        }
        
        private static HumanizationResult GetSuccessedResult(string content)
        {
            return new HumanizationResult {Successed = true, HumanizedContent = content};
        }
    }
}
