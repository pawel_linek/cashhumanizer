﻿using System;
using System.ServiceModel;
using System.Threading.Tasks;
using CashHumanizer.Shared.DataContract;
using CashHumanizer.Ui.CashHumanizerWcfService;

namespace CashHumanizer.Ui.Services
{
    public interface IHumanizatorService
    {
        Task<HumanizationResponse> Humanize(string cash);
    }

    public class HumanizatorService : IHumanizatorService
    {
        private readonly ICashHumanizerService _cashHumanizerWcfService;

        public HumanizatorService(ICashHumanizerService cashHumanizerWcfService)
        {
            _cashHumanizerWcfService = cashHumanizerWcfService;
        }

        public async Task<HumanizationResponse> Humanize(string cash)
        {
            try
            {
                return await _cashHumanizerWcfService.CashToHumanizeAsync(cash);
            }
            catch (EndpointNotFoundException)
            {
                return await Task.Run(() => new HumanizationResponse {ErrorMessage = "Service is dead."});
            }
            catch (Exception e)
            {
                return await Task.Run(() => new HumanizationResponse { ErrorMessage = e.Message });
            }
        }
    }
}