﻿using System.Globalization;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace CashHumanizer.Ui.Bahaviours
{
    public class PositiveCashBehaviour : Behavior<TextBox>
    {
        private const NumberStyles ValidNumberStyles = NumberStyles.AllowDecimalPoint |
                                                       NumberStyles.AllowThousands;

        private static readonly CultureInfo CultureInfoWithComma = CultureInfo.GetCultureInfo("PL-pl");
        private static char DecimalSeparator => CultureInfoWithComma.NumberFormat.NumberDecimalSeparator.First();
        private const decimal MaxValue = 999999999.99m;

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.PreviewTextInput += AssociatedObjectPreviewTextInput;
            AssociatedObject.PreviewKeyDown += AssociatedObjectPreviewKeyDown;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.PreviewTextInput -= AssociatedObjectPreviewTextInput;
            AssociatedObject.PreviewKeyDown -= AssociatedObjectPreviewKeyDown;
        }

        private static void AssociatedObjectPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void AssociatedObjectPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var constructedText = GetText(e.Text);
            if (IsValid(constructedText) == false)
            {
                e.Handled = true;
            }
        }

        private string GetText(string input)
        {
            var txt = AssociatedObject;

            var selectionStart = txt.SelectionStart;
            if (txt.Text.Length < selectionStart)
            {
                selectionStart = txt.Text.Length;
            }

            var selectionLength = txt.SelectionLength;
            if (txt.Text.Length < selectionStart + selectionLength)
            {
                selectionLength = txt.Text.Length - selectionStart;
            }

            var realtext = txt.Text.Remove(selectionStart, selectionLength);

            var caretIndex = txt.CaretIndex;
            if (realtext.Length < caretIndex)
            {
                caretIndex = realtext.Length;
            }

            var newtext = realtext.Insert(caretIndex, input);

            return newtext;
        }

        private static bool IsValid(string input)
        {
            if (input.ToCharArray().Count(x => x == DecimalSeparator) > 1)
            {
                return false;
            }

            var dollarsAndCents = input.Split(DecimalSeparator);
            var cents = input.Split(DecimalSeparator).LastOrDefault();
            if (dollarsAndCents.Length > 1 && cents != null && cents.Length > 2)
            {
                return false;
            }

            decimal d;
            return decimal.TryParse(input, ValidNumberStyles, CultureInfoWithComma, out d) && d <= MaxValue;
        }
    }
}