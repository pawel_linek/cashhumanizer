using CashHumanizer.Ui.CashHumanizerWcfService;
using CashHumanizer.Ui.Services;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace CashHumanizer.Ui.ViewModel
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<ICashHumanizerService>(() => new CashHumanizerServiceClient());
            SimpleIoc.Default.Register<IHumanizatorService>(() =>
            {
                var wcfService = SimpleIoc.Default.GetInstance<ICashHumanizerService>();
                return new HumanizatorService(wcfService);
            });
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }
    }
}