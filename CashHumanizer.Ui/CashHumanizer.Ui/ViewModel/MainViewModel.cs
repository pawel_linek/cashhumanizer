using System.Windows.Input;
using CashHumanizer.Ui.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace CashHumanizer.Ui.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IHumanizatorService _humanizatorService;

        public MainViewModel(IHumanizatorService humanizatorService)
        {
            _humanizatorService = humanizatorService;

            HumanizeCommand = new RelayCommand(HumanizeCash, CanExecute);
        }

        private string _cash;
        public string Cash
        {
            get { return _cash; }
            set
            {
                _cash = value;
                RaisePropertyChanged(nameof(Cash));
            }
        }

        public ICommand HumanizeCommand { get; private set; }

        private string _result;
        public string Result
        {
            get { return _result; }
            set
            {
                _result = value;
                RaisePropertyChanged(nameof(Result));
            }
        }

        private bool CanExecute()
        {
            return string.IsNullOrWhiteSpace(Cash) == false;
        }

        private async void HumanizeCash()
        {
            var taskResult = await _humanizatorService.Humanize(Cash);

            Result = taskResult.Successed ? taskResult.HumanizedCash : taskResult.ErrorMessage;
        }
    }
}