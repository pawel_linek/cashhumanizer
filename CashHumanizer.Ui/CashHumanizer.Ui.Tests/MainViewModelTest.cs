﻿using System.Threading.Tasks;
using CashHumanizer.Service.Test;
using CashHumanizer.Shared.DataContract;
using CashHumanizer.Ui.Services;
using CashHumanizer.Ui.ViewModel;
using FizzWare.NBuilder;
using NUnit.Framework;
using Rhino.Mocks;

namespace CashHumanizer.Ui.Tests
{
    [TestFixture]
    public class MainViewModelTest : TestWithSubject<MainViewModel>
    {
        [Test]
        public void CannotExecuteWhenCashIsNotProvided()
        {
            Subject.Cash = null;
            var canExecute = Subject.HumanizeCommand.CanExecute(null);

            Assert.IsFalse(canExecute);
        }

        [Test]
        public void CanExecuteWhenCashIsProvided()
        {
            Subject.Cash = "cash";
            var canExecute = Subject.HumanizeCommand.CanExecute(null);

            Assert.IsTrue(canExecute);
        }

        [Test]
        public void GetErrorMessageFromResult()
        {
            var humanizationResult = Builder<HumanizationResponse>.CreateNew()
                .With(r => r.ErrorMessage = "Error")
                .Build();
            AutoMocker.Get<IHumanizatorService>()
                .Stub(s => s.Humanize(Arg<string>.Is.Anything))
                .Return(new TaskFactory().StartNew(() => humanizationResult));

            Subject.Cash = "cash";
            Subject.HumanizeCommand.Execute(null);

            Assert.IsFalse(string.IsNullOrWhiteSpace(Subject.Result));
        }
    }
}