﻿using System;
using System.ServiceModel;
using System.Threading.Tasks;
using CashHumanizer.Service.Test;
using CashHumanizer.Shared.DataContract;
using CashHumanizer.Ui.CashHumanizerWcfService;
using CashHumanizer.Ui.Services;
using FizzWare.NBuilder;
using NUnit.Framework;
using Rhino.Mocks;

namespace CashHumanizer.Ui.Tests
{
    [TestFixture]
    public class HumanizatorServiceTest : TestWithSubject<HumanizatorService>
    {
        [Test]
        public async Task EdpointNotFoundExceptionExpected()
        {
            AutoMocker.Get<ICashHumanizerService>()
                .Stub(s => s.CashToHumanizeAsync(Arg<string>.Is.Anything))
                .Throw(new EndpointNotFoundException());

            var result = await Subject.Humanize("cash");

            Assert.IsFalse(result.Successed);
        }

        [Test]
        public async Task OtherExceptionExpected()
        {
            AutoMocker.Get<ICashHumanizerService>()
                .Stub(s => s.CashToHumanizeAsync(Arg<string>.Is.Anything))
                .Throw(new Exception());

            var result = await Subject.Humanize("cash");

            Assert.IsFalse(result.Successed);
        }

        [Test]
        public async Task HumanizationTestSuccess()
        {
            var response = Builder<HumanizationResponse>.CreateNew()
                .With(r => r.Successed = true)
                .With(r => r.HumanizedCash = "A lot of cash")
                .Build();
            AutoMocker.Get<ICashHumanizerService>()
                .Stub(s => s.CashToHumanizeAsync(Arg<string>.Is.Anything))
                .Return(new TaskFactory().StartNew(() => response));

            var result = await Subject.Humanize("cash");

            Assert.IsTrue(result.Successed);
        }
    }
}
